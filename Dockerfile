#FROM alpine:latest
FROM registry.gitlab.com/tr_devops/build_python_image/pypi_build:1.0.0
LABEL authors="Tobias Riazi <riazi.tobias@healthnow.org>"
LABEL description="Set Version - Python"

USER root

#RUN apk update
#RUN apk add python3 
#RUN apk add python3-dev 
#RUN apk add py3-pip
COPY requirements.txt .
RUN pip3 install -r requirements.txt
RUN rm requirements.txt

#RUN mkdir app
#WORKDIR /app
COPY scripts/. .

USER 1001
