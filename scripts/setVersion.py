#!/usr/bin/python3

import requests
import semantic_version
import sys, getopt

def getDatafromApi(url, token):
    header = {
        "Private-Token": token
    }
    res = requests.get(url, headers=header)
    res_x = res.json()
    return(res_x)

def main(argv):
    projectid = ""
    tokenvalue = ""
    branchname = ""
    commitmessage = ""
    pipelineid = ""
    try:
      opts, args = getopt.getopt(argv,"hp:t:b:c:l:",["projid=","token=","branch=","commitmsg=","pipelineid="])
    except getopt.GetoptError:
      print('setVersion.py -p <projectid> -t <token> -b <branch> -c <commitmsg> -l <pipelineid>')
      exit(1)
    for opt, arg in opts:
        if opt == '-h':
            print('setVersion.py -p <projectid> -t <token> -b <branch> -c <commitmsg> -l <pipelineid>')
            exit(1)
        elif opt in ("-p", "--projid"):
            projectid = arg
        elif opt in ("-t", "--token"):
            tokenvalue = arg
        elif opt in ("-b", "--branch"):
            branchname = arg
        elif opt in ("-c", "--commitmsg"):
            commitmessage = arg
        elif opt in ("-l", "--pipelineid"):
            pipelineid = arg
    url = f"https://gitlab.com/api/v4//projects/{projectid}/repository/tags?sort=desc"    
    try:
        res = getDatafromApi(url, tokenvalue)
    except:
        print("ERROR - getDatafromApi")
        exit(1)
    newVersion = setNewVersion(res, branchname, commitmessage)
    with open("stableVersion", "w") as ofStabelVersion:
        ofStabelVersion.write(str(newVersion))
    with open("Version", "w") as ofVersion:
        ofVersion.write(str(newVersion) + "-beta." + pipelineid)
    print(str(newVersion) + "-beta." + pipelineid)

def setNewVersion(tags, branchname, commitmessage):
    new_version = ""    
    if(len(tags) >= 1):
        current_version = semantic_version.Version(tags[0]['name'])
        if "major_bump" in commitmessage:
            new_version = current_version.next_major()
            return new_version
        if "hotfix" in branchname:
            new_version = current_version.next_patch()
            return new_version
        else:
            new_version = current_version.next_minor()
            return new_version
    else:
        new_version = semantic_version.Version("1.0.0")
    return new_version

if __name__ == "__main__":
    main(sys.argv[1:])
