#!/usr/bin/python3

import requests
import sys, getopt

def postDataToApi(url, token, version, refSha, description):
    headers = {
        'Content-Type': 'application/json',
        'PRIVATE-TOKEN': token
    }
    data = {
        'name': version,
        'tag_name': version,
        'ref': refSha,
        'description': description
    }    
    res = requests.post(url, headers=headers, json=data)    
    return(res)

def main(argv):
    projectid = ""
    tokenvalue = ""
    ShortCommitSha = ""
    committitle = ""
    version = ""
    try:
      opts, args = getopt.getopt(argv,"hp:t:s:c:v:",["projid=","token=","commitsha=","committitle=","version="])
    except getopt.GetoptError:
      print('tagRelease.py -p <projectid> -t <token> -s <commitsha> -c <committitle> -v <version>')
      exit(1)
    for opt, arg in opts:
        if opt == '-h':
            print('tagRelease.py -p <projectid> -t <token> -s <commitsha> -c <committitle> -v <version>')
            exit(1)
        elif opt in ("-p", "--projid"):
            projectid = arg
        elif opt in ("-t", "--token"):
            tokenvalue = arg
        elif opt in ("-s", "--commitsha"):
            ShortCommitSha = arg
        elif opt in ("-c", "--committitle"):
            committitle = arg
        elif opt in ("-v", "--version"):
            version = arg         
    url = f"https://gitlab.com/api/v4/projects/{projectid}/releases"
    print(url)
    try:        
        res = postDataToApi(url, tokenvalue, version, ShortCommitSha, committitle)
        print(res)
    except:
        print("ERROR - postDataToApi")
        exit(1)
    
if __name__ == "__main__":
    main(sys.argv[1:])
